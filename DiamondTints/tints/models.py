from django.db import models
from django.utils import timezone
from uuid import uuid4

# Create your models here.

class Cliente(models.Model):
    nombre = models.CharField(max_length=30)
    ciudad = models.CharField(max_length=30)
    rut = models.CharField(max_length=9)
    contacto = models.IntegerField(null=True)

    def __str__(self):
        return f'({self.cuidad}) {self.nombre}'

class Comprobante(models.Model):
    codigo = models.UUIDField(default=uuid4, editable=False)
    fecha = models.DateField(default=timezone.now)
    monto = models.FloatField(null=True, default=0.0)
    tipo_despacho = models.CharField(max_length=45)
    cliente = models.ForeignKey(Cliente, on_delete=None, default=None)

    def __str__(self):
        return self.codigo

class Producto(models.Model):
    nombre = models.CharField(max_length=45)
    precio = models.FloatField(null=True,default=0.0)
    stock = models.PositiveIntegerField(null=True, default=0)
    imagen = models.ImageField(upload_to='productos/', blank=True, null=True)

    def __str__(self):
        return self.nombre

class Encargo(models.Model):
    comprobante = models.ForeignKey(Comprobante, on_delete=None, default=None)
    precio_total =  models.FloatField(null=True,default=0.0)
    fecha_encargo = models.DateField(default=timezone.now)
    producto = models.ForeignKey(Producto, on_delete=None, default=None)

    def __str__(self):
        return f'{self.id} {self.comprobante}'

class Compra(models.Model):
 
    producto = models.ForeignKey(Producto, on_delete=None, default=None)
    fecha_compra = models.DateField(default=timezone.now)
    proveedor = models.CharField(max_length=45)
    valor = models.FloatField(null=True,default=0.0)

    def __str__(self):
        return f'{self.fecha_compra} {self.producto.nombre}'

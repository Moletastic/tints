from django.apps import AppConfig


class TintsConfig(AppConfig):
    name = 'tints'

# Generated by Django 2.1.5 on 2019-01-23 01:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tints', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='producto',
            name='imagen',
            field=models.ImageField(blank=True, null=True, upload_to='productos/'),
        ),
    ]
